#+TITLE: KT sessions by Madhavi Puliraju
#+DATE: [2019-11-29 Tue]
#+PROPERTY: results output
#+PROPERTY: exports code
#+SETUPFILE: ../org-templates/level-1.org
#+options: ^:nil
#+LATEX: Literal LaTeX code for export


* Introduction

  This document lists the plan, probable/required topic KT session,
  responsibilities, and expected person to take over the certain task.
  KT session will only target on certain tasks which were completely
  handled by the individual and others have partial or no idea on
  them.

* Plan
  This includes the tasks and reposibilitites handled by me and
  proposed name of engineers who must attend the KT session.  Related
  documentation link is mentioned and updates as per the requirements
  will be made.
** List of projects 
|------+------------------------------+--------------------------------+-------------------------------|
| S.No | Project Name                 | Documentation/repository links | Remarks                       |
|------+------------------------------+--------------------------------+-------------------------------|
|    1 | Outreach Portal              | [[https://github.com/vlead/outreach-portal][Outreach Portal]]                | Will be used till March, 2020 |
|------+------------------------------+--------------------------------+-------------------------------|
|    2 | ADS(Auto Deployment Service) | [[https://github.com/vlead/ovpl.git][ADS]]                            | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    3 | Analytics Service            | [[https://github.com/vlead/vlabs-analytics-service.git][Analytics-service]]              | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    4 | Analytics DB                 | [[https://github.com/vlead/analytics-db.git][Analytics-database-service]]     | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    5 | Feedback Service             | [[https://github.com/vlead/feedback-portal.git][Feedback-service]]               | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    6 | Lab Data Service (LDS)       | [[https://github.com/vlead/lab-data-service][Lab Data Service]]               | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    7 | Landing Pages                | [[https://github.com/vlead/vlabs-landing-pages][Vlabs-Landing-pages]]            | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    8 | Data Service Dashboard       | [[https://github.com/vlead/lab-data-service-dashboard.git][Lab Data Service Dashboard]]     | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|
|    9 | Lab Specifications           | [[https://github.com/vlead/lab-specifications][Lab-specifications]]             | Not in Use                    |
|------+------------------------------+--------------------------------+-------------------------------|

** Outreach Portal
*** Description of task
    
    This task will brief the following items

    1) Understanding of MVC AngularJS framework
    2) Implementation of Views, Models and Controllers
    3) Defining data models
    4) Implementation of back end and front end 

*** To be handed over to
    - Balamma (Primary responsibility)
    - Niranjan and Pavan

*** Documentation/repository links
    [[https://github.com/vlead/outreach-portal][Outreach Portal]]

*** Enhancements and future works
    Following are the list of future works
    1. [[https://gitlab.com/vlead-systems/systems-operations/issues/509][Backup files and database to a different machine]]
    2. [[https://github.com/vlead/outreach-portal/issues/304][Unable to deploy outreach portal on base4 machine]]
    3. [[https://github.com/vlead/outreach-portal/issues/227][Captcha has to be implemented along with Google OAuth]]
    4. [[https://github.com/vlead/outreach-portal/issues/174][Implement exception handling for front end]]   
    5. [[https://github.com/vlead/outreach-portal/issues/461][Need to upgrade python version as the support will get stopped by Jan 1st, 2020]]   



* Mom held during KT Sessions
|------+---------------------------------+----------------+-----------+------------------|
| S.no | KT Session                      | Handed over to | Attendees | Date             |
|------+---------------------------------+----------------+-----------+------------------|
|    1 | KT Session on requirements      | Balamma        | Balamma   | [2019-12-03 Tue] |
|      |                                 | Niranjan       | Niranjan  |                  |
|      |                                 |                | Sadhana   |                  |
|      |                                 |                | Raj       |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    2 | KT Session Understanding of     | Balamma        | Balamma   | [2019-12-04 Wed] |
|      | MVC - Angular JS framework      | Pavan          | Pavan     |                  |
|      |                                 |                | Sadhana   |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    3 | KT Session Defining Data        | Balamma        | Balamma   | [2019-12-05 Thu] |
|      | Models of Outreach Portal       | Niranjan       | Niranjan  |                  |
|      |                                 |                | Sadhana   |                  |
|      |                                 |                | Raj       |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    4 | KT Session: Outreach Backend    | Balamma        | Balamma   | [2019-12-06 Fri] |
|      |                                 | Niranjan       | Niranjan  |                  |
|      |                                 |                | Raj       |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    5 | KT Session: Outreach Frontend   | Balamma        | Balamma   | [2019-12-11 Wed] |
|      |                                 | Niranjan       | Niranjan  |                  |
|      |                                 |                | Sadhana   |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    6 | KT Session: Outreach Deployment | Balamma        | Balamma   | [2019-12-11 Wed] |
|      |                                 | Niranjan       | Niranjan  |                  |
|      |                                 | Pavan          | Sadhana   |                  |
|      |                                 |                | Pavan     |                  |
|------+---------------------------------+----------------+-----------+------------------|
|    7 | KT Session: Release Management  | Balamma        | Balamma   | [2019-12-11 Wed] |
|      |                                 | Niranjan       | Niranjan  |                  |
|      |                                 |                | Sadhana   |                  |
|      |                                 |                | Pavan     |                  |
|------+---------------------------------+----------------+-----------+------------------|


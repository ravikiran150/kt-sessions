#+TITLE: KT sessions by Sadhana Virupaksha
#+DATE: [2019-12-30 Mon]
#+PROPERTY: results output
#+PROPERTY: exports code
#+SETUPFILE: ../org-templates/level-1.org
#+options: ^:nil
#+LATEX: Literal LaTeX code for export

* Introduction
  This document lists the plan, probable/required topic KT
  session, responsibilities, and expected person to take
  over the certain task. KT session will only target on
  certain tasks which were completely handled by the
  individual and others have partial or no idea on them.

* Plan
  This includes the tasks and responsibilities handled by me
  and proposed name of engineers who must attend the KT
  session. Related documentation link is mentioned and
  updates as per the requirements will be made.

* Tasks and Responsibilities
** Experiment Development
*** Description of task
This task will brief the following items :
1. Experiment development workflow and architecture
2. Experiment development process
3. Implementation of experiment infrastructure/platform
4. Understanding the experiment build process
5. Understanding the process to test publish experiment
   infrastructure on cloud

*** To be handed over to
+ Thirumal
+ Raj
+ Ojas
+ Balamma
+ Niranjan

*** Documentation / Repository Link
+ [[https://gitlab.com/vlead-projects/experiments/index/blob/master/src/index.org][Index]] : This is the main repository which points to
  other repositories and documents which captures all the details related to experiment development process.
  
+ All the experiments are hosted on AWS : 
  1. [[http://exp-iiith.vlabs.ac.in][Data Structures]]      
  2. [[http://exp-iiith.vlabs.ac.in/ds/][Data Structures 1]]   
  3. [[http://exp-iiith.vlabs.ac.in/ds2/][Data Structures 2]]  

*** Future works
Following are the list of future works :
  1. Cross browser testing and fixes for data structures
     experiments
  2. Video recordings to be done for all the built
     experiments
  3. Footer service for experiment platform
  4. [[http://exp-iiith.vlabs.ac.in/ds/][Data Structures 1]] and [[http://exp-iiith.vlabs.ac.in/ds2/][Data Structures 2]] should be moved
     to [[vlabs.iiit.ac.in][vlabs.iiit.ac.in]]   
  5. Automate the experiments deployment process

*** Changes in ERE
Following are the changes to be in ere as per the KT
discussions :
1. Index repository of ere, which is used for building and
   publishing ere should be renamed to ere-builder

2. Location to publish the ere tar file should be defined in
   ere-builder config not in ere-publisher.

3. Should have a different catalog for
   org-exporters. Currently these are defined in
   org-exporters.

4. Change makefile of the experiment artefacts as it is
   using the old literate tools with the older version of
   org.

* KT Sessions
|------+-------------------------------------------------------------+----------------+-----------+------------------|
| S.NO | KT Session                                                  | Handed Over to | Attendees | Date             |
|------+-------------------------------------------------------------+----------------+-----------+------------------|
|   1. | KT session on experiment development workflow, architecture | Thirumal       | Thirumal  | [2020-01-01 Wed] |
|      | and experiment infrastructure implementation                | Raj            | Raj       |                  |
|      |                                                             | Ojas           | Ojas      |                  |
|      |                                                             | Balamma        | Balamma   |                  |
|      |                                                             | Niranjan       | Niranjan  |                  |
|------+-------------------------------------------------------------+----------------+-----------+------------------|
|   2. | KT session on experiment build process and                  | Thirumal       | Thirumal  | [2020-01-02 Thu] |
|      | process to test and publish infrastructure                  | Raj            | Raj       |                  |
|      |                                                             | Ojas           | Ojas      |                  |
|      |                                                             | Balamma        | Balamma   |                  |
|      |                                                             | Niranjan       | Niranjan  |                  |
|------+-------------------------------------------------------------+----------------+-----------+------------------|
